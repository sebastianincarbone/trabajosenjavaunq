package model;

import java.util.Date;

public class EmpleadoPermanente extends Empleado{

  //  ColavoradoresInternos

  private double sueldoBasico = 0;
  private int cantidadDeHijos = 0;
  private Date fechaDeIngreso;

  //  Constructores

  public EmpleadoPermanente(String nombre, String direccion, String estadoCivil, Date fechaDeNacimiento, Date fechaDeIngreso, int cantidadDeHijos, double sueldoBasico) {
    super(nombre, direccion, estadoCivil, fechaDeNacimiento);
    this.cantidadDeHijos = cantidadDeHijos;
    this.fechaDeIngreso = fechaDeIngreso;
    this.sueldoBasico = sueldoBasico;
  }

  //  MensajesPrivados

  private double plusPorCantidadDeHijos(){
    return this.cantidadDeHijos * 20 /*Pesos por cada hijo*/;
  }

  private double obraSocial(){
    return this.sueldoBruto() * 0.1 + this.plusPorCantidadDeHijos();
  }

  private double aportesJubilatorios(){
    return  this.sueldoBruto() * 0.15;
  }

  private double asignacionPorCónyuge(){
    if (super.estadoCivil().equals("Casado")) return 100/*Pesos por si es casado*/;
    return 0;
  }

  private double asignacionPorHijos(){
    return this.cantidadDeHijos * 150/*Pesos por cada hijo*/;
  }

  private double antigüedad(){
    return  (new Date().getYear() - this.fechaDeIngreso.getYear()) * 50/*Pesos por año de antigüedad*/;
  }

  //  MensajesImplementadosYAgregados

  @Override
  public void aumentarSueldoBasico(int montoEnPesos) {
    this.sueldoBasico += montoEnPesos;
  }

  @Override
  public double sueldoBasico() {
    return this.sueldoBasico;
  }

  @Override
  public double sueldoBruto() {
    return this.sueldoBasico + this.asignacionPorCónyuge()  + this.asignacionPorHijos() + this.antigüedad();
  }

  @Override
  public double sueldoNeto() {
    return this.sueldoBruto() - this.retenciones();
  }

  @Override
  public double retenciones() {
       return this.obraSocial() + this.aportesJubilatorios();
  }

  public int cantidadDeHijos() {
    return cantidadDeHijos;
  }

  public void aumentarCantidadDeHijos(int unNumeroDeHijos){
    this.cantidadDeHijos += unNumeroDeHijos;
  }

  @Override
  public String desgloce() {
    return "\"retencionesDeObraSocial\":{"
            +"\"obraSocial\":" + this.sueldoBruto() * 0.1
            + "\"plusPorCantidadDeHijos\":" + this.plusPorCantidadDeHijos()
            +"},"
            +"\"retencionesDeAportesJubilatorios\":{"
            +"\"aportesJubilatorios\":" + this.sueldoBruto() * 0.15
            +"},"
            +"\"sueldoBruto\":{"
            + "\"sueldoBasico\":" + this.sueldoBasico()
            + "\"asignacionPorCónyuge\":" + this.asignacionPorCónyuge()
            + "\"asignacionPorHijos\":" + this.asignacionPorHijos()
            +"}";
  }
}
