package model;

import java.util.Date;

public class EmpleadoTemporal extends Empleado {

  //  ColavoradoresInternos

  private double sueldoBasico = 0;
  private Date fechaDeFinDeDesignacionAPlantaTemporaria;
  private int cantidadDeHorasExtras = 0;

  //  Constructores

  public EmpleadoTemporal(String nombre, String direccion, String estadoCivil, Date fechaDeNacimiento, Date fechaDeFinDeDesignacionAPlantaTemporaria, double sueldoBasico) {
    super(nombre, direccion, estadoCivil, fechaDeNacimiento);
    this.fechaDeFinDeDesignacionAPlantaTemporaria = fechaDeFinDeDesignacionAPlantaTemporaria;
    this.sueldoBasico = sueldoBasico;
  }

  //  MensajesPrivados

  private double plusPorEdadParaObraSocial(){
    if (super.edad() > 50 /*Años*/) return 25/*Pesos*/;
    return 0;
  }

  private double plusDeHorasExtras(){
    return this.cantidadDeHorasExtras * 40/*Pesos por HoraExtra*/;
  }

  private double plusPorHorasExtraParaAportesJubilatorios(){
      return this.cantidadDeHorasExtras * 5 /*Pesos por cada horaExtra*/;
  }

  private double aportesJubilatorios(){
    return ((this.sueldoBruto() * 0.1) + this.plusPorHorasExtraParaAportesJubilatorios());
  }

  private double obraSocial(){
    return this.sueldoBruto() * 0.1 + this.plusPorEdadParaObraSocial();
  }

  //  MensajesImplementadosYAgregados

  @Override
  public void aumentarSueldoBasico(int montoEnPesos) {
    this.sueldoBasico += montoEnPesos;
  }

  @Override
  public double sueldoBasico() {
    return this.sueldoBasico;
  }

  @Override
  public double sueldoBruto() {
    return this.sueldoBasico + this.plusDeHorasExtras();
  }

  @Override
  public double sueldoNeto() {
    return this.sueldoBruto() - this.retenciones();
  }

  @Override
  public double retenciones() {
    return this.obraSocial() + this.aportesJubilatorios();
  }

  public Date fechaDeFinDeDesignacionAPlantaTemporaria() {
    return this.fechaDeFinDeDesignacionAPlantaTemporaria;
  }

  public int cantidadDeHorasExtras() {
    return this.cantidadDeHorasExtras;
  }

  public void aumentarCantidadDeHorasExtras(int unaCantidadDeHorasExtras) {
    this.cantidadDeHorasExtras += unaCantidadDeHorasExtras;
  }

  @Override
  public String desgloce() {
    return "\"retencionesDeObraSocial\":{"
            +"\"obraSocial\":" + this.sueldoBruto() * 0.1
            + "\"descuentoPorEdad\":" + this.plusPorEdadParaObraSocial()
            +"},"
            +"\"retencionesDeAportesJubilatorios\":{"
            +"\"aportesJubilatorios\":" + this.sueldoBruto() * 0.1
            + "\"aportesPorHorasExtra\":" + this.plusPorHorasExtraParaAportesJubilatorios()
            +"},"
            +"\"sueldoBruto\":{"
            + "\"sueldoBasico\":" + this.sueldoBasico()
            + "\"plusPorHorasExtra\":" + this.plusDeHorasExtras()
            +"}";
  }
}
