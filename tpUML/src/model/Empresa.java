package model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author sebaink
 *
 */
public class Empresa {
	
	private String nombreDeLaEmpresa;
	private String cuitDeLaEmpresa;
	private ArrayList<Empleado> empleados = new ArrayList<Empleado>();
	private ArrayList<Haber> registroDeHaberes = new ArrayList<Haber>();
	
	/**
	 * Esta empresa representa a un modelo de liquidaciones de sueldo
	 * @param nombreDeLaEmpresa:String y CUIT:String
	 */
	public Empresa(String nombreDeLaEmpresa, String cuitDeLaEmpresa) {
		this.nombreDeLaEmpresa = nombreDeLaEmpresa;
		this.cuitDeLaEmpresa = cuitDeLaEmpresa;
	}


	/**
	 * 
	 * @return retorna el nombre de la empresa en forma de String
	 */
	public String nombre() {
		return this.nombreDeLaEmpresa;
	}

	/**
	 * 
	 * @return retorna el cuit en forma de String
	 */
	public String cuit() {
		return this.cuitDeLaEmpresa;
	}
	
	public void contratar(Empleado unEmpleado) { this.empleados.add(unEmpleado);}

	public int  montoTotalDeDineroParaPagarSueldos() {
	    int  montoTotalDeDineroParaPagarSueldos = 0;
	    Iterator empleado = this.empleados.iterator();
	    while (empleado.hasNext()) montoTotalDeDineroParaPagarSueldos += ((Empleado) empleado.next()).sueldoNeto();
	    return montoTotalDeDineroParaPagarSueldos;
	}
	
	public int  montoTotalDeSueldosBruto() {
	    int  montoTotalDeSueldosBruto = 0;
	    Iterator empleado = this.empleados.iterator();
	    while (empleado.hasNext()) montoTotalDeSueldosBruto += ((Empleado) empleado.next()).sueldoBruto();
	    return montoTotalDeSueldosBruto;
	}
	
	public int  montoTotalDeRetenciones() {
	    int  montoTotalDeRetenciones = 0;
	    Iterator empleado = this.empleados.iterator();
	    while (empleado.hasNext()) montoTotalDeRetenciones += ((Empleado) empleado.next()).retenciones();
	    return montoTotalDeRetenciones;
	}
	
	public void  liquidacion() {
	    Iterator empleado = this.empleados.iterator();
	    
	    while (empleado.hasNext()) {
	      Empleado empleado1 = (Empleado) empleado.next();
	      System.out.println(new Haber(empleado1).toJSONString());
	      boolean add = this.registroDeHaberes.add(new Haber(empleado1));
	    }
	}
}
