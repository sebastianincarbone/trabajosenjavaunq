package model;

import java.util.Date;

public class EmpleadoContratado extends Empleado {

  private int sueldoBásico;
  private int numeroDeContrato;
  private String medioDePago;
  private int gastosAdministrativosContractuales = 50;

  public EmpleadoContratado(String nombre, String direccion, String estadoCivil, Date fechaDeNacimiento, int sueldoBásico, int númerodecontrato, String mediodePago) {
    super(nombre, direccion, estadoCivil, fechaDeNacimiento);
    this.sueldoBásico = sueldoBásico;
    numeroDeContrato = númerodecontrato;
    this.medioDePago = mediodePago;
  }

  public int gastosAdministrativosContractuales() {
    return gastosAdministrativosContractuales;
  }

  public void gastosAdministrativosContractuales(int gastosAdministrativosContractuales) {
    this.gastosAdministrativosContractuales = gastosAdministrativosContractuales;
  }

  public int numerodecontrato() {
    return numeroDeContrato;
  }

  public String mediodePago() {
    return medioDePago;
  }

  @Override
  public void aumentarSueldoBasico(int montoEnPesos) {
    this.sueldoBásico += montoEnPesos;
  }

  @Override
  public double sueldoBasico() {
    return this.sueldoBásico;
  }

  @Override
  public double sueldoBruto() {
    return this.sueldoBásico;
  }

  @Override
  public double retenciones() {
    return this.gastosAdministrativosContractuales;
  }

  @Override
  public double sueldoNeto() {
    return this.sueldoBásico - this.gastosAdministrativosContractuales;
  }

  @Override
  public String desgloce() {
    return   "\"retenciones\":{"
            +"\"gastosAdministrativosContractuales\":" + this.gastosAdministrativosContractuales
            +"},"
            +"\"sueldoBruto\":{"
            + "\"sueldoBasico\":" + this.sueldoBasico()
            +"}";
  }
}
