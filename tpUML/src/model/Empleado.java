package model;

import java.util.Date;

public abstract class Empleado {
//  ColavoradoresInternos

  private String nombre;
  private String direccion;
  private String estadoCivil;
  private Date fechaDeNacimiento;

//Constructor

public Empleado(String nombre, String direccion, String estadoCivil, Date fechaDeNacimiento) {
  this.nombre = nombre;
  this.direccion = direccion;
  this.estadoCivil = estadoCivil;
  this.fechaDeNacimiento = fechaDeNacimiento;
}
  
  //  MensajesImplementadas

  public String nombre() {
    return nombre;
  }

  public void nombre(String nombre) {
    this.nombre = nombre;
  }

  public String direccion() {
    return direccion;
  }

  public void direccion(String direccion) {
    this.direccion = direccion;
  }

  public String estadoCivil() {
    return estadoCivil;
  }

  public void estadoCivil(String estadoCivil) {
    this.estadoCivil = estadoCivil;
  }

  public Date fechaDeNacimiento() {
    return fechaDeNacimiento;
  }

  public void fechaDeNacimiento(Date fechaDeNacimiento) {
    this.fechaDeNacimiento = fechaDeNacimiento;
  }

  public int edad() {
	int edad = new Date().getYear() - this.fechaDeNacimiento.getYear();
    int auxDeEdad = new Date().getMonth() - this.fechaDeNacimiento.getMonth();

    if (auxDeEdad == 0) edad --;

    return edad;
  }

  //  MensajesParaImplementar

  public abstract void aumentarSueldoBasico(int montoEnPesos);

  public abstract double sueldoBasico();

  /** 
   * SueldoBruto
   *EmpleadoDePlantaPermanente
   *    Sueldo Básico
   *    Salario Familiar, que se compone de:
   *    Asignación por hijo: $150 por cada hijo.
   *    Asignación por cónyuge: $100 si tiene cónyuge
   *    Antigüedad: $50 por cada año de antigüedad.
   *
   *EmpleadoDePlantaTemporal
   *	  Sueldo Básico
   *	  Horas Extras: $40 por cada hora extra
   *
   * @return sueldo bruto: int
   */
  public abstract double sueldoBruto();
  
  /**
   * Retencinoes
   *EmpleadoDePlantaPermanente
   *    Obra Social: 10% de su sueldo bruto + $20 por cada hijo.
   *    Aportes Jubilatorios: 15% de su sueldo bruto*
   *
   *EmpleadoDePlantaTemporal
   *    Obra Social: 10% de su sueldo bruto + $25 si supera los 50 años
   *    Aportes Jubilatorios: 10% de su sueldo bruto + $5 por cada hora extra.
   *  
   * @return retenciones : int
   */
  public abstract double retenciones();
  
  public abstract double sueldoNeto();

  public abstract String desgloce();
	
}
