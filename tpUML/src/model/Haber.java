package model;

import java.util.Date;

public class Haber {
  //  ColavoradoresInternos

  private Empleado empleado;

  //  Constructor

  public Haber(Empleado empleado) {
    this.empleado = empleado;
  }

  //  MensajesPublicos

  public String toJSONString() {
    return "\"Haber\":{"
            + "\"nombre\":" + empleado.nombre()
            + "\"direccion \":" + empleado.direccion()
            + "\"fechaDeEmisión\":" + new Date().toGMTString()
            + "\"sueldoBruto\":" + empleado.sueldoBruto()
            + "\"sueldoNeto\":" + empleado.sueldoNeto()
            + "\"desgloce\":" + "{"+ empleado.desgloce() +"}"
            + "}";
  }
}
