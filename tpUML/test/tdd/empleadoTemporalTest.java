package tdd;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;

import model.Empleado;
import model.EmpleadoTemporal;

class empleadoTemporalTest {


	  @Test
	  void test00_ContructorDeEmpleadoPermanente() {
	    EmpleadoTemporal unEmpleadoTemporal = new EmpleadoTemporal("sebastian","direc","Soltero",new Date(1995,04,17),new Date(2018,11,11),8000);
	    assertTrue(unEmpleadoTemporal.cantidadDeHorasExtras() == 0);
	  }

	  @Test
	  void test01_SueldoBasico() {
	    Empleado unEmpleadoTemporal = new EmpleadoTemporal("sebastian","direc","Soltero",new Date(1995,04,17),new Date(2018,11,11),8000);

	    assertTrue(unEmpleadoTemporal.sueldoBasico() == 8000);
	  }

	  @Test
	  void test02_AumentarSueldoBasico() {
	    Empleado unEmpleadoTemporal = new EmpleadoTemporal("sebastian","direc","Soltero",new Date(1995,04,17),new Date(2018,11,11),8000);

	    unEmpleadoTemporal.aumentarSueldoBasico(1000 /*Pesos*/);

	    assertTrue(unEmpleadoTemporal.sueldoBasico() == 9000);
	  }

	  @Test
	  void test03_SueldoBruto() {
	    Empleado unEmpleadoTemporal = new EmpleadoTemporal("sebastian","direc","Soltero",new Date(1995,04,17),new Date(2018,11,11),8000);

	    assertTrue(unEmpleadoTemporal.sueldoBruto() == 8000);
	  }

	  @Test
	  void test04_Retenciones() {
	    Empleado unEmpleadoTemporal = new EmpleadoTemporal("sebastian","direc","Soltero",new Date(1995,04,17),new Date(2018,11,11),8000);

	    assertTrue(unEmpleadoTemporal.retenciones() == 1600);
	  }

	  @Test
	  void test05_SueldoNeto() {
	    Empleado unEmpleadoTemporal = new EmpleadoTemporal("sebastian","direc","Soltero",new Date(1995,04,17),new Date(2018,11,11),8000);

	    assertTrue(unEmpleadoTemporal.sueldoNeto() == 6400);
	  }

}
