package tdd;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Date;
import org.junit.jupiter.api.Test;
import model.Empleado;
import model.EmpleadoPermanente;

class empleadoPermanente {

	@Test
	  void test00_ContructorDeEmpleadoPermanente() {
	    EmpleadoPermanente unEmpleadoPermanente = new EmpleadoPermanente("sebastian","direc","Soltero",new Date(1995,04,17),new Date(),0,8000);

	    assertTrue(unEmpleadoPermanente.cantidadDeHijos() == 0);
	  }

	  @Test
	  void test01_SueldoBasico() {
	    Empleado unEmpleadoPermanente = new EmpleadoPermanente("sebastian","direc","Soltero",new Date(1995,04,17),new Date(),0,8000);

	    assertTrue(unEmpleadoPermanente.sueldoBasico() == 8000);
	  }

	  @Test
	  void test02_AumentarSueldoBasico() {
	    Empleado unEmpleadoPermanente = new EmpleadoPermanente("sebastian","direc","Soltero",new Date(1995,04,17),new Date(),0,8000);

	    unEmpleadoPermanente.aumentarSueldoBasico(1000 /*Pesos*/);

	    assertTrue(unEmpleadoPermanente.sueldoBasico() == 9000);
	  }

	  @Test
	  void test03_SueldoBruto() {
	    Empleado unEmpleadoPermanente = new EmpleadoPermanente("sebastian","direc","Soltero",new Date(1995,04,17),new Date(),0,8000);

	    assertTrue(unEmpleadoPermanente.sueldoBruto() == 8000);
	  }

	  @Test
	  void test04_Retenciones() {
	    Empleado unEmpleadoPermanente = new EmpleadoPermanente("sebastian","direc","Soltero",new Date(1995,04,17),new Date(),0,8000);

	    assertTrue(unEmpleadoPermanente.retenciones() == 2000);
	  }

	  @Test
	  void test05_SueldoNeto() {
	    Empleado unEmpleadoPermanente = new EmpleadoPermanente("sebastian","direc","Soltero",new Date(1995,04,17),new Date(),0,8000);

	    assertTrue(unEmpleadoPermanente.sueldoNeto() == 6000);
	  }
}
