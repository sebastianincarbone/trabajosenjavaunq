package tdd;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;

import model.Empleado;
import model.EmpleadoContratado;
import model.EmpleadoPermanente;
import model.EmpleadoTemporal;
import model.Empresa;

class testEmpresa {

@Test
	void test00ParaElConstructorDeUnaEmpresa() {
		//Definicion de variables para construir una empresa
		String nombreDeLaEmpresa = "unNombre";
		String cuitDeLaEmpresa = "12-122333443-1";
		//Constructor de una empresa
		Empresa unaEmpresa = new Empresa(nombreDeLaEmpresa, cuitDeLaEmpresa);
		//Retorno de metodos de la empresa deseados testear
		String nombre = unaEmpresa.nombre();
		String cuit = unaEmpresa.cuit();
		//asserts por el nombre y el cuit de la empresa
		assertTrue(nombre.equals(nombreDeLaEmpresa));
		assertTrue(cuit.equals(cuitDeLaEmpresa));
	}

	  Empresa unaEmpresa(){
	    return new Empresa("Dell", "xx-xxxxxxxx-x");
	  }
	
	  @Test
	  void test05_unaEmpresaMePuedeDecirElMontoTotalDeDineroParaPagarSueldos() {
	    Empresa unaEmpresa = this.unaEmpresa();
	
	    unaEmpresa.contratar(new EmpleadoTemporal("1","11","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("2","22","Casado",new Date(1995,04,17),new Date(),0,10000));
	unaEmpresa.contratar(new EmpleadoTemporal("3","33","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("4","44","Casado",new Date(1995,04,17),new Date(),2,10000));
	unaEmpresa.contratar(new EmpleadoTemporal("5","55","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("6","66","Casado",new Date(1995,04,17),new Date(),1,10000));
	unaEmpresa.contratar(new EmpleadoContratado("6", "66", "Casado", new Date(1995, 04, 17), 10000, 1, "cheques"));
	
	assertTrue(unaEmpresa.montoTotalDeDineroParaPagarSueldos() == 52152/*Pesos*/);
	  }
	
	  @Test
	  void test06_unaEmpresaMePuedeDecirElMontoTotalDeSueldosBruto() {
	    Empresa unaEmpresa = this.unaEmpresa();
	
	    unaEmpresa.contratar(new EmpleadoTemporal("1","11","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("2","22","Casado",new Date(1995,04,17),new Date(),0,10000));
	unaEmpresa.contratar(new EmpleadoTemporal("3","33","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("4","44","Casado",new Date(1995,04,17),new Date(),2,10000));
	unaEmpresa.contratar(new EmpleadoTemporal("5","55","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("6","66","Casado",new Date(1995,04,17),new Date(),1,10000));
	unaEmpresa.contratar(new EmpleadoContratado("6", "66", "Casado", new Date(1995, 04, 17), 10000, 1, "cheques"));
	
	assertTrue(unaEmpresa.montoTotalDeSueldosBruto() == 64750/*Pesos*/);
	  }
	
	  @Test
	  void test07_unaEmpresaMePuedeDecirElMontoTotalDeRetenciones() {
	    Empresa unaEmpresa = this.unaEmpresa();
	
	    unaEmpresa.contratar(new EmpleadoTemporal("1","11","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("2","22","Casado",new Date(1995,04,17),new Date(),0,10000));
	unaEmpresa.contratar(new EmpleadoTemporal("3","33","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("4","44","Casado",new Date(1995,04,17),new Date(),2,10000));
	unaEmpresa.contratar(new EmpleadoTemporal("5","55","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
	unaEmpresa.contratar(new EmpleadoPermanente("6","66","Casado",new Date(1995,04,17),new Date(),1,10000));
	unaEmpresa.contratar(new EmpleadoContratado("6", "66", "Casado", new Date(1995, 04, 17), 10000, 1, "cheques"));
	
	assertTrue(unaEmpresa.montoTotalDeRetenciones() == 12597/*Pesos*/);
	  }
	
	  @Test
	  void test08_Liquidaciones() {
	    Empresa unaEmpresa = this.unaEmpresa();

	    unaEmpresa.contratar(new EmpleadoTemporal("1","11","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
unaEmpresa.contratar(new EmpleadoPermanente("2","22","Casado",new Date(1995,04,17),new Date(),0,10000));
unaEmpresa.contratar(new EmpleadoTemporal("3","33","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
unaEmpresa.contratar(new EmpleadoPermanente("4","44","Casado",new Date(1995,04,17),new Date(),2,10000));
unaEmpresa.contratar(new EmpleadoTemporal("5","55","soltero",new Date(1995,04,17),new Date(2018,11,20),8000));
unaEmpresa.contratar(new EmpleadoPermanente("6","66","Casado",new Date(1995,04,17),new Date(),1,10000));
unaEmpresa.contratar(new EmpleadoContratado("6", "66", "Casado", new Date(1995, 04, 17), 10000, 1, "cheques"));

    unaEmpresa.liquidacion();
  }

}
