package controller;

import java.io.IOException;

import model.Model;
import view.View;

public class Controller {

	private View view;
	private Model model;
	private String comand;
	
	public Controller(View view, Model model) {
		super();
		this.view = view;
		this.model = model;
	}

	public void init() {
		System.out.println("hi wellcome!");	
		do {
			try {
				this.comand = view.read();
				this.execute(this.comand);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}while(!this.comand.equals("exit"));
	}

	private void execute(String texto) {
		switch (texto.split(" ")[0]) {
		case "console":
			System.out.println("hi! my console is great!");
			break;

		case "sumar":
			this.model.setPrimerNumero(texto.split(" ")[1]);
			this.model.setSegundoNumero(texto.split(" ")[2]);
			System.out.println(this.model.sumar());
			break;

		case "restar":
			this.model.setPrimerNumero(texto.split(" ")[1]);
			this.model.setSegundoNumero(texto.split(" ")[2]);
			System.out.println(this.model.restar());
			break;

		case "multiplicar":
			this.model.setPrimerNumero(texto.split(" ")[1]);
			this.model.setSegundoNumero(texto.split(" ")[2]);
			System.out.println(this.model.multiplicar());
			break;

		case "dividir":
			this.model.setPrimerNumero(texto.split(" ")[1]);
			this.model.setSegundoNumero(texto.split(" ")[2]);
			System.out.println(this.model.dividir());
			break;
			
		case "exit":
			System.out.println("bye bye!");
			break;

		default:
			System.out.println("pleace tip help, but i dont know what you say :(");
			break;
		}
	}
	
}
