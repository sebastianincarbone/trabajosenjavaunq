package bootstraper;

import controller.Controller;
import model.Model;
import view.View;

public class bootstraper {
	
	public static void main(String args[]) {
		Controller controller = new Controller(new View(), new Model());
		controller.init();
	}
}
