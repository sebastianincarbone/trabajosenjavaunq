package model;

public class Model {
	private int primerNumero;
	private int segundoNumero;
	private int resultado;

	
	public int sumar() {
		return this.primerNumero + this.segundoNumero;
	}
	
	public int restar() {
		return this.primerNumero - this.segundoNumero;
	}

	public String multiplicar() {
		return Double.toString(this.primerNumero * this.segundoNumero);
	}

	public String dividir() {
		return Double.toString(this.primerNumero / this.segundoNumero);
	}
	
	
	
	public int getPrimerNumero() {
		return primerNumero;
	}
	public void setPrimerNumero(String primerNumero) {
		this.primerNumero = Integer.parseInt(primerNumero);
	}
	public int getSegundoNumero() {
		return segundoNumero;
	}
	public void setSegundoNumero(String segundoNumero) {
		this.segundoNumero = Integer.parseInt(segundoNumero);
	}
	public int getResultado() {
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

	
	
}
