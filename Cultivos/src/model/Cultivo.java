package model;

public class Cultivo {
	/**
	 * tipo de cultivo, puede ser soja, trigo o mixto
	 */
	private TipoDeCultivo tipoDeCultivo;

	public Cultivo(TipoDeCultivo tipoDeCultivo) {
		this.tipoDeCultivo = tipoDeCultivo;
	}

	public Double getGananciaAnual() {
		return this.tipoDeCultivo.getGananciaAnual();
	}
		
	
}
