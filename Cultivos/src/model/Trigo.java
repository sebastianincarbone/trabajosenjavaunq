package model;

public class Trigo implements TipoDeCultivo {

	private Double gananciaAnual;
	
	public Trigo(Double gananciaAnual) {
		this.gananciaAnual = gananciaAnual;
	}

	@Override
	public Double getGananciaAnual() {
		return this.gananciaAnual;
	}

}
