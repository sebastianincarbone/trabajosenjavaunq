package model;

public class Soja implements TipoDeCultivo {

	private Double gananciaAnual;
	
	public Soja(Double gananciaAnual) {
		this.gananciaAnual = gananciaAnual;
	}

	@Override
	public Double getGananciaAnual() {
		return this.gananciaAnual;
	}

}
