package model;

public class CultivoMixto implements TipoDeCultivo {

	private TipoDeCultivo parcela1;
	private TipoDeCultivo parcela2;
	private TipoDeCultivo parcela3;
	private TipoDeCultivo parcela4;
	
	public CultivoMixto(TipoDeCultivo parcela1, TipoDeCultivo parcela2, TipoDeCultivo parcela3, TipoDeCultivo parcela4) {
		this.parcela1 = parcela1;
		this.parcela2 = parcela2;
		this.parcela3 = parcela3;
		this.parcela4 = parcela4;
	}
	
	@Override
	public Double getGananciaAnual() {
		return this.calcularGanacia();
	}

	private Double calcularGanacia() {	
		return (this.parcela1.getGananciaAnual() + 
				this.parcela2.getGananciaAnual() + 
				this.parcela3.getGananciaAnual() + 
				this.parcela4.getGananciaAnual()) /4;
	}

}
