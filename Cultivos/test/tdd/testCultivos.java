package tdd;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.*;

import model.Cultivo;
import model.CultivoMixto;
import model.Soja;
import model.TipoDeCultivo;
import model.Trigo;

public class testCultivos {

	private Cultivo cultivoSoloDeSoja;
	private Cultivo cultivoSoloDeTrigo;
	private Cultivo elCampoDeAmalia;
	private Cultivo campo2;
	private TipoDeCultivo soja;
	private TipoDeCultivo trigo;
	private TipoDeCultivo mixto;
	private TipoDeCultivo mixtoConMixtoAdentro;
	
	@Before
	public void setUp() {
		/*-------------------------------------------------------------------------------------*/
		this.soja = new Soja(2000d);
		/*-------------------------------------------------------------------------------------*/
		this.trigo= new Trigo(1200d);
		/*-------------------------------------------------------------------------------------*/
		this.mixto= new CultivoMixto(this.soja, this.trigo,this.soja,this.trigo);
		/*-------------------------------------------------------------------------------------*/
		this.mixtoConMixtoAdentro= new CultivoMixto(this.soja, this.trigo, this.soja, this.mixto);
		/*-------------------------------------------------------------------------------------*/
		
		this.cultivoSoloDeSoja = new Cultivo(this.soja);
		this.cultivoSoloDeTrigo = new Cultivo(this.trigo);
		this.elCampoDeAmalia= new Cultivo(this.mixto);
		this.campo2= new Cultivo(this.mixtoConMixtoAdentro);
	}
	
	@Test
	public void unCultivoSoloDeSojaTendraUnaGananciaAnualDe2000() {
		Double expected = 2000d;
		Double actual = this.cultivoSoloDeSoja.getGananciaAnual();
		assertEquals(expected, actual);
	}
	
	@Test
	public void unCultivoSoloDeTrigoTendraUnaGananciaAnualDe1200() {
		Double expected = 1200d;
		Double actual = this.cultivoSoloDeTrigo.getGananciaAnual();
		assertEquals(expected, actual);
	}
	
	@Test
	public void elCampoDeAmaliaTendraUnaGananciaAnualDe1600() {
		Double expected = 1600d;
		Double actual = this.elCampoDeAmalia.getGananciaAnual();
		assertEquals(expected, actual);
	}
	
	@Test
	public void campo2TendraUnaGananciaAnualDe1700() {
		Double expected = 1700d;
		Double actual = this.campo2.getGananciaAnual();
		assertEquals(expected, actual);
	}

}
