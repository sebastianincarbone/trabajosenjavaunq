package model;
/**
 * 
 * @author sebaink
 *
 */
public class Producto {
	private String nombre;
	private TipoDeProducto tipoDeProducto;
	/**
	 * contructor de un nuevo producto
	 * @param String que reprecenta le nombre del producto
	 * @param Double que reprecenta el precio del producto
	 * @param TipoDeProducto establece el tipoDeProducto y se encarga de aplicar indicaciones especiales por cada caso
	 */
	public Producto(String nombre, Double precio, TipoDeProducto tipoDeProducto) {
		this.nombre = nombre;
		this.tipoDeProducto = tipoDeProducto;
		this.tipoDeProducto.setPrecio(precio);
	}
	/**
	 * 
	 * @return String que reprecenta el nombre del producto
	 */
	public String getNombre() {
		return this.nombre;
	}
	/**
	 * 
	 * @return Double que reprecenta el precio del producto con las condiciones que impone el tipo de producto establecido
	 */
	public Double getPrecio() {
		return this.tipoDeProducto.getPrecio();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj.getClass().equals(Producto.class)) {
			Producto unProducto = (Producto) obj;
			return this.nombre.equals(unProducto.getNombre());
		}
		return false;
	}

}
