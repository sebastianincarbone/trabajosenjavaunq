package model;

public abstract class TipoDeProducto {
	
	private Double precio;
	
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	
	protected Double getPrecioProtected() {
		return this.precio;
	}
	
	public abstract Double getPrecio();
	
}
