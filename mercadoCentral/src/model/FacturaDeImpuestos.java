package model;
/**
 * 
 * @author sebaink
 *
 */
public class FacturaDeImpuestos implements IFactura{

	private Double tasaDelServicio;
	/**
	 * contructor de una nueva factura de impuestos
	 * @param Double representando a la tasa del servicio que hay que abonar
	 */
	public FacturaDeImpuestos(Double tasaDelServicio) {
		this.tasaDelServicio = tasaDelServicio;
	}
	/* (non-Javadoc)
	 * @see model.IFactura#getMontoACobrar()
	 */
	@Override
	public Double getMontoACobrar() {
		return this.tasaDelServicio;
	}

	
}
