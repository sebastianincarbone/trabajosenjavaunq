package model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 
 * @author sebaink
 *
 */
public class Inventario {

	ArrayList<Items> inventario;
	/**
	 * contructor de un nuevo inventario con un array de items
	 */
	public Inventario() {
		inventario = new ArrayList<Items>();
	}
	/**
	 * reitero sobre la lista para setear y buscar cantidad
	 * 
	 * @param Producto unProducto
	 * @param int unaCantidadDelProducto
	 * @return retorna la cantidad del producto actualizada
	 */
	private int iteracion(Producto unProducto, int unaCantidadDelProducto) {
		Iterator<Items> item = this.inventario.iterator();
		while(item.hasNext()) {
			Items itemLocal = item.next();
			if(itemLocal.esDelProducto(unProducto)) {
				itemLocal.setCantidadDeProducto(unaCantidadDelProducto);				
				return itemLocal.getCantidadDelProducto();
			}
		}
		if(!item.hasNext()) {
			this.inventario.add(new Items(unProducto, unaCantidadDelProducto));
			return unaCantidadDelProducto;
		}
		return 0;
	}
	/**
	 * setea la cantidad de producto, en caso de no tenerlo agrega un nuevo item a la lista
	 * @param Producto unProducto
	 * @param int unaCantidadDelProducto
	 */
	public void setCandidadDe(Producto unProducto,int unaCantidadDelProducto) {
		this.iteracion(unProducto, unaCantidadDelProducto);
	}
	/**
	 * descuenta del inventario una cantidad especifica del producto
	 * @param Producto unProducto
	 * @param int unaCantidadVendidaDelProducto
	 */
	public void venderXCandidadDe(Producto unProducto, int unaCantidadVendidaDelProducto) {
		this.iteracion(unProducto, unaCantidadVendidaDelProducto * -1);
	}
	/**
	 * recive como parametro un producto para machear la cantidad y retornarla
	 * @param Producto unProducto
	 * @return un int reprecentando la cantidad de producto que se encuentra registrada
	 */
	public int getCantidadDe(Producto unProducto) {
		return this.iteracion(unProducto, 0);
	}

}
