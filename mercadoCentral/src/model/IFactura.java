package model;

public interface IFactura {
	/**
	 * aqui se calcula cuanto es lo que el usuario/ posedor de dicha factura debe abonar
	 * @return un monto en pesos 
	 */
	public Double getMontoACobrar();

}
