/**
 * 
 */
package model;

/**
 * @author sebaink
 *
 */
public class FacturaDeServicios implements IFactura {

	private Double costoPorUnidadConsumida;
	private Double cantidadDeUnidadesConsumidas;
	/**
	 * 
	 * @param costoPorUnidadConsumida
	 * @param cantidadDeUnidadesConsumidas
	 */
	public FacturaDeServicios(Double costoPorUnidadConsumida, Double cantidadDeUnidadesConsumidas) {
		this.costoPorUnidadConsumida = costoPorUnidadConsumida;
		this.cantidadDeUnidadesConsumidas = cantidadDeUnidadesConsumidas;
	}
	/**
	 * 
	 * @return
	 */
	public Double getCostoPorUnidadConsumida() {
		return costoPorUnidadConsumida;
	}
	/**
	 * 
	 * @return
	 */
	public Double getCantidadDeUnidadesConsumidas() {
		return cantidadDeUnidadesConsumidas;
	}
	/* (non-Javadoc)
	 * @see model.IFactura#getMontoACobrar()
	 */
	@Override
	public Double getMontoACobrar() {
		return this.cantidadDeUnidadesConsumidas * this.costoPorUnidadConsumida;
	}

}
