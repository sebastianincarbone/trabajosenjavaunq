package model;

public class CajaDeVenta {
	private Double montoACobrar;
	/**
	 * 
	 * @param montoDeUnItemDeLaVenta
	 */
	public CajaDeVenta() {
		super();
		montoACobrar = 0d;
	}
	/**
	 * 
	 * @param montoDeUnItemDeLaVenta
	 */
	public void agregarVenta(Double montoDeUnItemDeLaVenta) {
		this.montoACobrar += montoDeUnItemDeLaVenta;
	}
	/**
	 * 
	 * @return
	 */
	public Double cobrar() {
		return this.montoACobrar;
	}
	/**
	 * 
	 * @param factura
	 * @return
	 */
	public Double cobrar(IFactura factura) {
		this.montoACobrar = factura.getMontoACobrar();
		return this.montoACobrar;
	}
	/**
	 * 
	 */
	public void cerrarVenta() {
		this.montoACobrar = 0d;
	}
}
