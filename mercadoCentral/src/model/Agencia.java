package model;

import java.util.ArrayList;;

public class Agencia implements IAgencia{

	private ArrayList<IFactura> registroDePagoDeServicios;
	
	public Agencia() {
		this.registroDePagoDeServicios = new ArrayList<IFactura>();
	}

	@Override
	public void registrarPago(IFactura factura) {
		this.registroDePagoDeServicios.add(factura);		
	}

	public int getCantidadDeFacturasCobradas() {
		return this.registroDePagoDeServicios.size();
	}

}
