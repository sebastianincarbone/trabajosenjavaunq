package model;
/**
 * 
 * @author sebaink
 *
 */
public class SuperMercado {

	private CajaDeVenta caja;
	private Inventario inventario;
	private Agencia agenciaRecaudadora;
	/**
	 * constructor de un nuevo SuperMercado donde instancia una caja nueva, y un inventario nuevo
	 */
	public SuperMercado(){
		this.caja = new CajaDeVenta();
		this.inventario = new Inventario();
	}
	/**
	 * constructor de un nuevo SuperMercado donde instancia una caja nueva, y un inventario nuevo
	 * @param agenciaRecaudadora 
	 */
	public SuperMercado(Agencia agenciaRecaudadora){
		this.caja = new CajaDeVenta();
		this.inventario = new Inventario();
		this.agenciaRecaudadora = agenciaRecaudadora;
	}
	/**
	 * descuenta del Inventario una cantidad del produto pedido
	 * @param Producto unProducto
	 * @param int unaCantidadVendidaDelProducto
	 */
	private void descontarDelInventario(Producto unProducto,int unaCantidadVendidaDelProducto) {
		this.inventario.venderXCandidadDe(unProducto, unaCantidadVendidaDelProducto);
	}
	/**
	 * agrega un Nuevo Producto al inventario con una cantidad
	 * @param unProducto
	 * @param unaCantidadDelProducto
	 */
	public void agregarUnNuevoProducto(Producto unProducto ,int unaCantidadDelProducto) {
		this.inventario.setCandidadDe(unProducto, unaCantidadDelProducto);
	}

	/**
	 * agrega una cantidad al producto pedido, pero en caso de no tenerlo
	 * agrega un Nuevo Producto al inventario con una cantidad
	 * @param Producto unProducto
	 * @param int unaCantidadDelProducto
	 */
	public void aumentarCantidadDeProducto(Producto unProducto ,int unaCantidadDelProducto) {
		this.inventario.setCandidadDe(unProducto, unaCantidadDelProducto);
	}
	/**
	 * PREGUNTAR!
	 * @param producto
	 * @param precio
	 */
	public void aumentarPrecio(Producto producto ,int precio) {
		/**
		 * 
		 * este metodo no se implemeta ya que el controller es el que tiene 
		 * la lista de productos_?
		 * 
		 */
	}
	/**
	 * realiza la venta del producto con una cantidad
	 * descontando de stock
	 * realizando los calculos para llevar a cabo la cuenta en la caja
	 * 
	 * @param Producto unProducto
	 * @param int unaCantidadASerVendida
	 */
	public void venderProducto(Producto unProducto ,int unaCantidadASerVendida) {
		this.descontarDelInventario(unProducto, unaCantidadASerVendida);
		this.caja.agregarVenta(unProducto.getPrecio() * unaCantidadASerVendida);
	}
	/**
	 * entrega el monto a pagar 
	 * reinicia la caja para que pueda comenzar una nueva cuenta
	 * 
	 * @return Double monto a pagar por el cliente
	 */
	public Double cobrar() {
		Double tmp = this.caja.cobrar();
		this.caja.cerrarVenta();

		return tmp;
	}
	/**
	 * entrega el monto a pagar 
	 * reinicia la caja para que pueda comenzar una nueva cuenta
	 * 
	 * @return Double monto a pagar por el cliente
	 */
	public Double cobrar(IFactura factura) {
		Double tmp = this.caja.cobrar(factura);
		this.agenciaRecaudadora.registrarPago(factura);
		this.caja.cerrarVenta();

		return tmp;
	}
	/**
	 * admite un Producto para retornar su cantidad en el inventario
	 * 
	 * @param Producto unProducto
	 * @return int reprecentando la cantidad del producto registrado en el sueperMercado
	 */
	public int cantidadDeProducto(Producto unProducto) {
		return this.inventario.getCantidadDe(unProducto);
	}

}
