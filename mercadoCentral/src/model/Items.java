package model;

public class Items {
	private Producto unProducto;
	private int cantidadDelProducto;
	/**
	 * contructor de un nuevo Item que es donde se relacionan los productos con sus cantidades
	 * 
	 * @param Producto unProducto
	 * @param int unaCantidadDelProducto
	 */
	public Items(Producto unProducto,int unaCantidadDelProducto) {
		this.cantidadDelProducto = unaCantidadDelProducto;
		this.unProducto = unProducto;
	}
	/**
	 * 
	 * @return el producto en cuention
	 */
	public Producto getUnProducto() {
		return unProducto;
	}
	/**
	 * suma a la cantidad del producto una cantidad asignada
	 * @param int reprecentando la cantidad del producto
	 */
	public void setCantidadDeProducto(int cantidad) {
		this.cantidadDelProducto += cantidad;
	}
	/**
	 * 
	 * @return la cantidad del producto que se encuenta en el registro
	 */
	public int getCantidadDelProducto() {
		return cantidadDelProducto;
	}
	/**
	 * indica si este es el producto que contiene este item
	 * @param Producto unProducto
	 * @return booleano true || false
	 */
	public boolean esDelProducto(Producto unProducto) {
		return this.unProducto.equals(unProducto);
	}
	
}
