package model;
/**
 * 
 * @author sebaink
 *
 */
public class ProductoDeCoop extends TipoDeProducto{
	private Double descuentoDel;
	/**
	 * constructor del producto de la cooperativa por defecto el descuento es del 10%
	 */
	public ProductoDeCoop() {
		this.descuentoDel = (1 - (10d / 100));
	}
	/**
	 * seteo del descuento por si es que disminuye o aumenta
	 * @param unMontoADescontar este para metro es un numero que reprecenta el descuento
	 */
	public void setDescuento(Double unMontoADescontar) {
		this.descuentoDel = (1 - (unMontoADescontar / 100));
	}
	
	@Override
	public Double getPrecio() {
		return this.precioConDescuento();
	}
	/**
	 * 
	 * @return precio con el descuento aplicado
	 */
	private Double precioConDescuento() {
		return this.descuentoDel * super.getPrecioProtected();
	}
}
