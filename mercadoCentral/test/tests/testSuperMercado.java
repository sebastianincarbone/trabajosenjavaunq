package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import model.*;

public class testSuperMercado {
	
	private SuperMercado superMercado;
	private Producto arroz;
	
	@Before
	public void setUp() throws Exception {
		arroz = new Producto("arroz",100d/*PesosPorKilo*/,new ProductoDeEmpresa());
		superMercado = new SuperMercado();
		superMercado.agregarUnNuevoProducto(arroz, 20/*kilogramos*/);
	}

	@Test
	public void test01_UnSupermercadoAlNoIngresarNingunaCompraElMontoAPagarDelClienteEsCero() {
		Double cantidadDePlata = this.superMercado.cobrar();
		Double expected = 0d;
		assertEquals(expected,cantidadDePlata);
	}

	@Test
	public void test02_AunSupermercadoQueSeStockeaDe20KilosDeArrozLosTineQueTener() {
		int cantidadDeAroz= this.superMercado.cantidadDeProducto(this.arroz);
		int expected = 20;
		assertEquals(expected, cantidadDeAroz);
	}

	@Test
	public void test03_UnSupermercadoAlVenderUnKiloDeArrozElTotalAPagarSeraCienPesos() {
		this.superMercado.venderProducto(arroz, 1 /*kiloGramo*/);
		Double cantidadDePlata = this.superMercado.cobrar();
		Double expected = 100d;
		assertEquals(expected, cantidadDePlata);
	}

	@Test
	public void test04_UnSupermercadoAlVenderUnKiloDeArrozSeQuedaCon19KilosComoInventario() {
		this.superMercado.venderProducto(arroz, 1 /*kiloGramo*/);
		int cantidadDeArroz = this.superMercado.cantidadDeProducto(this.arroz);
		int expected = 19;
		assertEquals(expected, cantidadDeArroz);
	}
}
