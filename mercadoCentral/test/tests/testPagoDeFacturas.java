package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import model.*;

public class testPagoDeFacturas {
	private IFactura facturaDeServicio;
	private IFactura facturaDeImpuesto;
	private SuperMercado superMercado;
	private Agencia agenciaRecaudadora;

	@Before
	public void setUp() throws Exception {
		this.facturaDeServicio = new FacturaDeServicios(100d, 10d);
		this.facturaDeImpuesto = new FacturaDeImpuestos(1000d);
		this.agenciaRecaudadora = new Agencia();
		this.superMercado = new SuperMercado(agenciaRecaudadora);
	}

	@Test
	public void test01UnaFacturaDeServiciosCuyoValorPorUnidadEs10yLasUnidadesConsumidas100ElMontoAPagarSera1000() {
		Double expected = 1000d;
		Double actual = this.facturaDeServicio.getMontoACobrar();
		assertEquals(expected, actual);
	}

	@Test
	public void test02UnaFacturaDeImpuestosCuyoMontoACobrarEs1000ElMontoAPagarSera1000() {
		Double expected = 1000d;
		Double actual = this.facturaDeImpuesto.getMontoACobrar();
		assertEquals(expected, actual);
	}

	@Test
	public void test03UnaAgenciaQueNoHaRegistradoNingunPagoNoTieneNinguPago() {
		int expected = 0;
		int actual = this.agenciaRecaudadora.getCantidadDeFacturasCobradas();
		assertEquals(expected, actual);
	}

	@Test
	public void test04UnSuperMercadoPuedeRealizarElCobroDeUnaFacturaYLaAgenciaRecaudadoraTendra1Registro() {
		Double montoACobrarActual = this.superMercado.cobrar(facturaDeServicio);
		Double montoACobrarEsperado = 1000d;
		assertEquals(montoACobrarEsperado, montoACobrarActual);
		int expected = 1;
		int actual = this.agenciaRecaudadora.getCantidadDeFacturasCobradas();
		assertEquals(expected, actual);
	}
}
