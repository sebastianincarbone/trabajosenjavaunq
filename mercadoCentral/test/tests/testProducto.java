package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.*;

public class testProducto {

	private Producto arroz;
	private Producto pan;
	
	@Before
	public void setUp() throws Exception {
		this.arroz = new Producto("arroz", 50d /*PesosPorKilogramo*/, new ProductoDeEmpresa());
		this.pan   = new Producto("pan", 100d /*PesosPorKilogramo*/, new ProductoDeCoop());
	}

	@Test
	public void test01ElPrecioDelArrozEs50Pesos() {
		Double precioDelArroz = this.arroz.getPrecio();
		assertEquals(precioDelArroz, Double.valueOf(50));
	}
	
	@Test
	public void test02ElPrecioDelPanTieneQueTenerUnDescuentoDel10Porciento() {
		Double precioDelPan = this.pan.getPrecio();
		assertEquals(precioDelPan, Double.valueOf(90));
	}
	
}
