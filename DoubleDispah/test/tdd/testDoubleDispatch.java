package tdd;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Usuario;

public class testDoubleDispatch {

	private Usuario unUsuario;

	@Test
	public void test1() {
		this.unUsuario = new Usuario(20);
		
		assertEquals("Experto", unUsuario.getNivel());
	
	}

	@Test
	public void test2() {
		this.unUsuario = new Usuario(10);
		
		assertEquals("Basico", unUsuario.getNivel());
	
	}

}
