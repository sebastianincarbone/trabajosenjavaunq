package model;

public class NivelBasico extends INivel {
	
	private final Integer valorDeReferencia = 10;
		
	@Override
	public Boolean puedeHacerseCargo(Integer unNumero) {
		return unNumero == this.valorDeReferencia;
	}

	@Override
	public String getNivel() {
		return "Basico";
	}

}
