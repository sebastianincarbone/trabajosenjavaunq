package model;

public class NivelExperto extends INivel {
	
	private final Integer valorDeReferencia = 20;

	@Override
	public Boolean puedeHacerseCargo(Integer unNumero) {
		return unNumero == this.valorDeReferencia;
	}

	@Override
	public String getNivel() {
		return "Experto";
	}

}
