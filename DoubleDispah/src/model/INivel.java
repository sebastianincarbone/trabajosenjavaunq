package model;

public abstract class INivel {
	
	public abstract Boolean puedeHacerseCargo(Integer unNumero);
	
	public abstract String getNivel();
}
