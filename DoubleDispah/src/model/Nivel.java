package model;

import java.util.ArrayList;

public class Nivel {

	private ArrayList<INivel> nivelesPosibles;
	
	public Nivel() {
		this.nivelesPosibles = new ArrayList<INivel>();
		this.nivelesPosibles.add(new NivelBasico());
		this.nivelesPosibles.add(new NivelExperto());
	}
	
	public String getNivel(Usuario unUsuario) {
		return this.nivelEncargado(unUsuario.getNumero()).getNivel();	
	}
	
	private INivel nivelEncargado(Integer unNumero) {
		for(INivel unNivel : this.nivelesPosibles) {
			if(unNivel.puedeHacerseCargo(unNumero)) {
				return unNivel;
			}
		}
		System.out.println("nivel no encontrado");
		return null;
	}
}
