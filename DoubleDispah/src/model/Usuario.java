package model;

public class Usuario {
	
	private Integer unNumero;
	private Nivel nivel;

	public Usuario(Integer unNumero) {
		this.nivel = new Nivel();
		this.unNumero = unNumero;
	}
	
	public void setNumero(Integer unNumero) {
		this.unNumero = unNumero;
	}
	
	public Integer getNumero() {
		return this.unNumero;
	}
	
	public String getNivel() {
		return this.nivel.getNivel(this);
	}
}
