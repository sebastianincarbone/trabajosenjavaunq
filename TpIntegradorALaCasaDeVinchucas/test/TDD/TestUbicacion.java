package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.Ubicacion;

public class TestUbicacion {

	private String longitud;
	private String latitud;
	
	@Before
	public void SetUp(){
		this.longitud = "123";
		this.latitud = "13";
		
	}

	@Test
	public void Tets01UnaUbicacionPuedeSerInstanciadaPorUnaLatitudYUnaLongitud() {
		Ubicacion unaUbcacion = new Ubicacion(this.latitud, this.longitud);
		assertTrue(true);
	}
	
	@Test
	public void Tets02UnaUbicacionPuedeRetornarSuLatitud() {
		Ubicacion unaUbcacion = new Ubicacion(this.latitud, this.longitud);
		
		String actual = unaUbcacion.getLatitud();
		String expected = this.latitud;
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void Tets02UnaUbicacionPuedeRetornarSuLongitud() {
		Ubicacion unaUbcacion = new Ubicacion(this.latitud, this.longitud);
		
		String actual = unaUbcacion.getLongitud();
		String expected = this.longitud;
		
		assertEquals(expected, actual);
	}

}
