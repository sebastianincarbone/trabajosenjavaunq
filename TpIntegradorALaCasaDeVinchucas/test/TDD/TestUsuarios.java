package TDD;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.NivelDeConocimiento;
import Model.Usuario;

/**
 * @author sebaink
 *
 */
public class TestUsuarios {
	
	/**
	 * variables para los test
	 */
	private Usuario unUsuario;
	private String aliasDeUsuario;	
	
	@Before
	public void setUp() {
		this.aliasDeUsuario = "Cosme Fulanito";
		this.unUsuario = new Usuario(this.aliasDeUsuario);
	}
	
	@Test
	public void test1UnUsuarioPuedeSerInstanciadoConUnAlias(){
		String expected = this.unUsuario.getAliasDeUsuario();
		String actual = this.aliasDeUsuario;
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void test2UnUsuraioPuedeVerificarUnaMuestra(){
		/*
		 * Aca es donde puedo usar mokito (?
		 * */
	}
	
	@Test
	public void test3UnUsuraioTieneUnNivelDeConocimiento(){
		NivelDeConocimiento expected;
		NivelDeConocimiento actual;
		assertEquals(expected, actual);
	}
	
}
