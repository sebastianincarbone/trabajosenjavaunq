/**
 * 
 */
package Model;

/**
 * @author sebaink
 *
 */
public abstract class NivelDeConocimiento {
	protected Integer experiencia;

	public Integer getExperiencia() {
		return experiencia;
	}

	public void subirExperiencia(Integer unaCantidadDeExperiencia) {
		this.experiencia += unaCantidadDeExperiencia; 
	}

	public void bajarExperiencia(Integer unaCantidadDeExperiencia) {
		this.experiencia -= unaCantidadDeExperiencia;		
	}

	public abstract String getNivelDeConocimiento();	

}
