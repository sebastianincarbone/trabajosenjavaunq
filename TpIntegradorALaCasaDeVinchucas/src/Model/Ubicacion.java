package Model;

public class Ubicacion {
	/**
	 * variables que reprecentan la latitud y la longitud de la aplicacion
	 */
	private String latitud;
	private String longitud;
	/**
	 * constructor de una Ubicacion admite como parametro:
	 * @param un String que reprecenta la latitud
	 * @param un String que reprecenta la longitud
	 */
	public Ubicacion(String latitud, String longitud) {
		super();
		this.latitud = latitud;
		this.longitud = longitud;
	}
	/**
	 * retorno de la latitud de la ubicacion
	 * @return un String que reprecenta la latitud
	 */
	public String getLatitud() {
		return latitud;
	}
	/**
	 * retorno de la longitud de la ubicacion
	 * @return un String que reprecenta la longitud
	 */
	public String getLongitud() {
		return longitud;
	}	
	
}
