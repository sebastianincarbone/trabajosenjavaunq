/**
 * 
 */
package Model;

/**
 * @author sebaink
 *
 */
public class Usuario {
	private String aliasDeUsuario;
	
	public Usuario(String unAliasDeUsuario) {
		this.aliasDeUsuario = unAliasDeUsuario;
	}
	
	public String getAliasDeUsuario() {
		return this.aliasDeUsuario;
	}
}
