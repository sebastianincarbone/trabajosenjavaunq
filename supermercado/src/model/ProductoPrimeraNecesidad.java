package model;
/**
 * 
 * @author sebaink
 *
 */
public class ProductoPrimeraNecesidad extends Producto{
	/**
	 * Constructor. crea una nueva instancia de un ProductoDePrimeraNecesidad con los parametros recibidos
	 * y calcula un descuento extra que se aplica al precio del producto.
	 * 
	 * @param nombre String
	 * @param precio double
	 * @param esParteDeLosPreciosCuidados boolean
	 * @param conUnDescuentoDe double
	 */
	public ProductoPrimeraNecesidad(String nombre, double precio, boolean esParteDeLosPreciosCuidados, double conUnDescuentoDe) {
		super(nombre, precio, esParteDeLosPreciosCuidados);
		this.setPrecioConUnDescuentoDe(precio, conUnDescuentoDe);
	}
	/**
	 * Constructor. crea una nueva instancia de un ProductoDePrimeraNecesidad con los parametros recibidos.
	 * 
	 * @param nombre String
	 * @param precio double
	 * @param esParteDeLosPreciosCuidados boolean
	 */
	public ProductoPrimeraNecesidad(String nombre, double precio, boolean esParteDeLosPreciosCuidados) {
		super(nombre, precio, esParteDeLosPreciosCuidados);
	}
	
}
