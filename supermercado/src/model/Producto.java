package model;
/**
 * 
 * @author sebaink
 *
 */
public class Producto {
	/**
	 * valor del descuento por precio cuidado
	 */
	private final double descuentoPorPrecioCuidado = 10.0;
	/**
	 * nombre del producto
	 */
	private String nombre;
	/**
	 * precio del producto
	 */
	private double precio;
	/**
	 * este campo indica si el producto es o no parte de los precios cuidados
	 */
	private boolean esParteDeLosPreciosCuidados;
	/**
	 * Constructor. crea una instancia de producto con los parametros recibidos.
	 * 
	 * @param nombre String
	 * @param precio double
	 * @param esParteDeLosPreciosCuidados boolean
	 */
	public Producto(String nombre, double precio, boolean esParteDeLosPreciosCuidados) {
		if (esParteDeLosPreciosCuidados)
			this.setPrecioConUnDescuentoDe(precio, descuentoPorPrecioCuidado);
		else
			this.setPrecio(precio);
		this.setNombre(nombre);
		this.setEsPrecioCuidado(esParteDeLosPreciosCuidados);
	}
	/**
	 * Constructor. crea una instancia de producto con los parametros recibidos.
	 * En este caso se toma por sabido que el producto no forma parte de los precios cuidados.
	 * 
	 * @param nombre String
	 * @param precio double
	 */
	public Producto(String nombre, double precio) {
		this.setPrecio(precio);
		this.setNombre(nombre);
		this.setEsPrecioCuidado(false);
	}
	
	/** Getters & Setters **/
	
	private void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private void setEsPrecioCuidado(boolean esParteDeLosPreciosCuidados) {
		this.esParteDeLosPreciosCuidados = esParteDeLosPreciosCuidados;
	}

	private void setPrecio(double precio) {
		this.precio = precio;
	}
	/**
	 * toma el precio y un valor a descontar para realizar el calculo y guardar el precio
	 * con el descuento aplicado
	 * 
	 * @param precio double
	 * @param descuento double
	 */
	protected void setPrecioConUnDescuentoDe(double precio, double descuento) {
		double descuentoParaAPlicar = 1 - (descuento / 100);
		double precioConDescuento = precio * descuentoParaAPlicar;
		this.precio = precioConDescuento;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPrecio() {
		return precio;
	}
	/**
	 * indica si forma parte de la lista de precios cuidados
	 * @return boolean
	 */
	public boolean esPrecioCuidado() {
		return this.esParteDeLosPreciosCuidados;
	}
	/**
	 * mensaje que permite aumentar el precio del producto un monto de dinero especifico
	 * @param unMontoEnPesos double
	 */
	public void aumentarPrecio(double unMontoEnPesos) {
		this.precio += unMontoEnPesos;
	}
}
