package model;

import java.util.ArrayList;
import java.util.Iterator;
/**
 * 
 * @author sebaink
 *
 */
public class Supermercado {
	/**
	 * nombre del supermercado
	 */
	private String nombre;
	/**
	 * direccion del supermercado
	 */
	private String direccion;
	/**
	 * listado de todos los productos del supermercado
	 */
	private ArrayList catalogo;

	/**
	 * Constructor. crea una instancia de supermercado con los parametros recibidos.
	 * 
	 * @param nombre String
	 * @param direccion String
	 */
	public Supermercado(String nombre, String direccion) {
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.catalogo = new ArrayList<Producto>();
	}
	
	/** Getters & Setters **/
	
	public String getNombre() {
		return nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	private void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getCantidadDeProductos() {
		return this.catalogo.size();
	}
	/**
	 * agrega el producto del parametro al catalogo
	 * 
	 * @param unProducto Producto
	 */
	public void agregarProducto(Producto unProducto) {
		this.catalogo.add(unProducto);		
	}
	/**
	 * calcula la cantidad de plata en productos que tiene el catalogo del supermercado
	 * 
	 * @return double
	 */
	public Double getPrecioTotal() {
		Double precioTotal = 0d;
		Iterator<Producto> producto = this.catalogo.iterator();
		while(producto.hasNext()) {
			precioTotal += producto.next().getPrecio();
		}
		return precioTotal;
	}

}
