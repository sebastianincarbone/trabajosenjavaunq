package tdd;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Cliente;

public class ClienteTest {

	@Test
	public void test00ContructorDeUnCliente() {
		Cliente sebastianIncarbone = new Cliente("Sebastian", "Incarbone", "El Pato Berazategui", 23, new Double(15000)); 
		String expecteds = sebastianIncarbone.getNombre();
		String actuals = "Sebastian";
		assertEquals(expecteds, actuals);
	}

}
