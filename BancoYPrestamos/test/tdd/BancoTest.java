package tdd;

import static org.junit.Assert.*;

import javax.xml.crypto.Data;

import org.junit.*;

import model.*;

public class BancoTest {

	private Banco unBanco;
	private Cliente sebastianIncarbone;
	private Cliente unCliente;

	@Before
	public void setUp() throws Exception {
		this.unBanco = new Banco();
		this.sebastianIncarbone = new Cliente("Sebastian", "Incarbone", "Berazategui", 23, new Double(25000)); 
	}

	@Test
	public void test01unBancoPuedeAgregarUnNuevoClienteASuListaDeClientes() {
		unBanco.agregarUnNuevoCliente(sebastianIncarbone);
		Boolean condition = unBanco.esCliente(sebastianIncarbone);
		assertTrue(condition);
	}

	@Test
	public void test02unClienteDelBancoPuedeSolicitarUnCreditoPresonal() {
		TipoDeCredito unPrestamoPersonal = new CreditoPersonal(sebastianIncarbone,new Double(12000), 12);
		Boolean condition = unBanco.solicitarCredito(unPrestamoPersonal);
		assertTrue(condition);
	}

	@Test
	public void test03unClienteDelBancoPuedeSolicitarUnCreditoHipotecario() {
		Propiedad propiedadDeSebastianIncarbone = new Propiedad(sebastianIncarbone, new Double(1000000000));
		TipoDeCredito unPrestamoHipotecario= new CreditoHipotecario(sebastianIncarbone,new Double(1000000), 80, propiedadDeSebastianIncarbone);
		Boolean condition = unBanco.solicitarCredito(unPrestamoHipotecario);
		assertTrue(condition);
	}
	
	
}
