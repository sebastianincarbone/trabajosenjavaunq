package model;

import java.util.ArrayList;

public class Banco {

	private ArrayList<Cliente> registroDeClientes;
	private ArrayList<TipoDeCredito> registroDeSolicitudesCrediticias;

	public Banco() {
		super();
		this.registroDeClientes = new ArrayList<Cliente>();
		this.registroDeSolicitudesCrediticias = new ArrayList<TipoDeCredito>();
	}
	
	public void agregarUnNuevoCliente(Cliente unCliente) {
		this.registroDeClientes.add(unCliente);
	}
	
	public Boolean esCliente(Cliente unCliente) {
		return this.registroDeClientes.contains(unCliente);
	}

	public Boolean solicitarCredito(TipoDeCredito unaSolicitudDePrestamo) {
		return unaSolicitudDePrestamo.evaluarCredito();
	}

}
