package model;

public class Propiedad {
	private Double valor;
	private Cliente titular;
	
	public Propiedad(Cliente titlular,Double valorDeLaPropiedad) {
		this.valor = valorDeLaPropiedad;
		this.titular = titlular;
	}
	
	public Cliente getTitular() {
		return titular;
	}

	public Double getValor() {
		return valor;
	}
}
