package model;

public class CreditoHipotecario extends TipoDeCredito{
	
	private Propiedad propiedadEnGarantia;

	public CreditoHipotecario(Cliente unCliente, Double unMontoAPrestar, Integer plazoEnMeses, Propiedad propiedadEnGarantia) {
		super(unCliente, unMontoAPrestar, plazoEnMeses);
		this.propiedadEnGarantia = propiedadEnGarantia;
	}

	@Override
	public Boolean evaluarCredito() {
		/**
		 * Para ser aceptadas, las solicitudes de créditos hipotecarios requieren que el monto de la cuota no
		 * supere el 50% de los ingresos mensuales del titular, que el monto total solicitado no sea mayor al 70% 
		 * del valor fiscal de la garantía, y que la persona no supere los 65 años de edad antes de terminar de
		 * pagar el crédito.
		 */
		return this.evaluarCredito(super.getCliente());
	}

	private Boolean evaluarCredito(Cliente unCliente) {
		return this.evaluarSueldoAnual(super.getMontoDelCredito())
				&& evaluarEdadDelCliente(unCliente.getEdad(), super.getPlazoEnMesesDelCredito())
				&& evaluarSueldoMensual(unCliente.getSueldoNetoMensual(),super.montoDeLaCuotaMensual());
	}

	private Boolean evaluarSueldoAnual(Double montoDelCredito) {
		return montoDelCredito*0.7 <=  this.propiedadEnGarantia.getValor();
	}

	private Boolean evaluarEdadDelCliente(Integer edadDelClienteActual, Integer plazoEnMesesDelCredito) {
		return edadDelClienteActual+(plazoEnMesesDelCredito/12) <= 65;
	}

	private Boolean evaluarSueldoMensual(Double sueldoNetoMensual,Double montoDeCuotaMensual) {
		return sueldoNetoMensual*0.5 >= montoDeCuotaMensual;
	}
}
