package model;

public abstract class TipoDeCredito {

	private Cliente cliente;
	private Double montoAPrestar;
	private Integer plazoEnMeses;

	public TipoDeCredito(Cliente unCliente, Double montoAPrestar, Integer plazoEnMeses) {
		this.cliente = unCliente;
		this.montoAPrestar = montoAPrestar;
		this.plazoEnMeses = plazoEnMeses;
	}
	
	protected Cliente getCliente() {
		return this.cliente;
	}

	protected Integer getPlazoEnMesesDelCredito() {
		return this.plazoEnMeses;
	}
	protected Double getMontoDelCredito() {
		return this.montoAPrestar;
	}

	public Double montoDeLaCuotaMensual() {
		return this.montoAPrestar / this.plazoEnMeses;
	}
	
	public abstract Boolean evaluarCredito();

}
