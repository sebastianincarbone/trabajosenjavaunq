package model;

public class Cliente {

	private String  nombre;
	private String  apellido;
	private String  direccion;
	private Integer edad;
	private Double sueldoNetoMensual ;
	private Double sueldoNetoAnual;

	public Cliente(String nombre, String apellido, String direccion, Integer edad, Double sueldoNetoMensual) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.edad = edad;
		this.sueldoNetoMensual = sueldoNetoMensual;
		this.setSueldoNetoAnual( sueldoNetoMensual );
	}

	private void setSueldoNetoAnual(Double sueldoNetoMensual) {
		this.sueldoNetoAnual = sueldoNetoMensual * 12;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getApellido() {
		return this.apellido;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public Integer getEdad() {
		return this.edad;
	}

	public Double getSueldoNetoMensual() {
		return this.sueldoNetoMensual;
	}

	public Double getSueldoNetoAnual() {
		return this.sueldoNetoAnual;
	}

}
