package model;

public class CreditoPersonal extends TipoDeCredito{

	private final Double montoMinimoDelSueldoNetoAnual = 15000d;

	public CreditoPersonal(Cliente unCliente, Double unMontoAPrestar, Integer plazoEnMeses) {
		super(unCliente, unMontoAPrestar, plazoEnMeses);
	}

	@Override
	public Boolean evaluarCredito() {
		/**
		 * Las solicitudes para los créditos personales requieren que el solicitante tenga ingresos anuales 
		 * por al menos $15000, y que el monto de la cuota no supere el 70% de sus ingresos mensuales.
		 */
		return this.evaluarCredito(super.getCliente());
	}
	
	private Boolean evaluarCredito(Cliente unCliente) {
		return this.evaluarSueldoAnual(unCliente.getSueldoNetoAnual()) 
				&& evaluarSueldoMensual(unCliente.getSueldoNetoMensual(),super.montoDeLaCuotaMensual());
	}

	private Boolean evaluarSueldoAnual(Double sueldoNetoAnual) {
		return sueldoNetoAnual >= this.montoMinimoDelSueldoNetoAnual;
	}

	private Boolean evaluarSueldoMensual(Double sueldoNetoMensual,Double montoDeCuotaMensual) {
		return sueldoNetoMensual*0.7 >= montoDeCuotaMensual;
	}
}
