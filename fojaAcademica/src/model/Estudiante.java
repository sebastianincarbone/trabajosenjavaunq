package model;

import java.time.LocalDate;

public class Estudiante {

	private int legajo;
	private int documento;
	private String nombre;
	private String apellido;
	
	private LocalDate fechaDeNacimiento;

	public Estudiante(String nombre, String apellido,int legajo, int documento, LocalDate fechaDeNacimiento) {
		this.setLegajo(legajo);
		this.setNombre(nombre);
		this.setApellido(apellido);
		this.setDocumento(documento);
		this.setFechaDeNacimiento(fechaDeNacimiento);
	}
	
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setDocumento(int documento) {
		this.documento = documento;
	}

	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public int getDocumento() {
		return documento;
	}

	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public int edad() {
		return this.calcularEdad();
	}

	private int calcularEdad() {
		LocalDate fechaActual = LocalDate.now();
		int dias = fechaActual.getDayOfYear() - this.fechaDeNacimiento.getDayOfYear();
		int meses = fechaActual.getMonthValue() - this.fechaDeNacimiento.getMonthValue();
		int años = fechaActual.getYear() - this.fechaDeNacimiento.getYear();
		if (this.yaCumplioAños(dias,meses)) return años;
		return años - 1;
	}

	private boolean yaCumplioAños(int dias, int meses) {
		return dias > 0 && meses> 0;
	}

	public int getLegajo() {
		return legajo;
	}

}
