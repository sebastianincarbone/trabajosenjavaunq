package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class FojaAcademica {
	
	private Estudiante estudiante;
	private Carrera carrera;
	
	public FojaAcademica(String nombreDeCarrera, String nombre, String apellido,int legajo, int documento, LocalDate fechaDeNacimiento) {
		this.estudiante = new Estudiante(nombre, apellido, legajo, documento, fechaDeNacimiento);
		this.carrera = new Carrera(nombreDeCarrera);
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public Carrera getCarrera() {
		return carrera;
	}
	
	public void agregarMateria(String nombre, int creditos, Horario horario) {
		TipoDeMateria tipoDeMateria = new MateriaCursada(horario);
		this.carrera.agregarMateria(new Materia(nombre, creditos, tipoDeMateria));
	}
	
	public void agregarMateria(String nombre, int creditos) {
		TipoDeMateria tipoDeMateria = new MateriaNoCursada();
		this.carrera.agregarMateria(new Materia(nombre, creditos, tipoDeMateria));
	}

	private void establecerHorariosPara(String nombre, String dia, LocalTime horaDeIncio, LocalTime horaDeCierre) {
		this.carrera.getMateria(nombre).getHorarios().setDiaYHora(dia, horaDeIncio, horaDeCierre);
	}
	
	public void getMateria(String nombre) {
		this.carrera.getMateria(nombre);
	}
}
