package model;

import java.time.LocalTime;
import java.util.ArrayList;

public class Materia {

	private String nombre;
	private int creditos;
	private TipoDeMateria tipoDeMateria;

	public Materia(String nombre,int creditos, TipoDeMateria tipoDeMateria) {
		this.creditos = creditos;
		this.nombre = nombre;
		this.tipoDeMateria = tipoDeMateria;
	}

	public String getNombre() {
		return this.nombre;
	}
	
	public int getCreditos() {
		return this.creditos;
	}
	
	public void agregarNota(Integer nota) {
		this.tipoDeMateria.agregarNota(nota);
	}

	public void setHorarios(Horario horarios) {
		this.tipoDeMateria.setHorarios(horarios);
	}
	
	public void setNotaFinal(int notaFinal) {
		this.tipoDeMateria.setNotaFinal(notaFinal);;
	}
	
	public int getNotaFinal() {
		return this.tipoDeMateria.getNotaFinal();
	}
	
	public ArrayList<Integer> getNotas() {
		return this.tipoDeMateria.getNotas();
	}
	
	public Horario getHorarios() {
		return this.tipoDeMateria.getHorarios();
	}
	
	public LocalTime getHoraDeInicio(String dia) {
		return this.tipoDeMateria.getHoraDeInicio(dia);
	}
	
	public LocalTime getHoraDeCierre(String dia) {
		return this.tipoDeMateria.getHoraDeCierre(dia);
	}	
	
}
