package model;

import java.time.LocalTime;
import java.util.ArrayList;

public class MateriaNoCursada extends TipoDeMateria{
	
	public MateriaNoCursada() {
		super();
	}
	
	@Override
	public void agregarNota(Integer nota) {}

	@Override
	public void setHorarios(Horario horarios) {}

	@Override
	public void setNotaFinal(int notaFinal) {}

	@Override
	public int getNotaFinal() {	return 0; }

	@Override
	public ArrayList<Integer> getNotas() { return null; }

	@Override
	public Horario getHorarios() { return null; }

	@Override
	public LocalTime getHoraDeInicio(String dia) { return null; }

	@Override
	public LocalTime getHoraDeCierre(String dia) { return null;	}

	public boolean isMateriaNoCursada() { return true; }
	
	public boolean isMateriaCursada() { return false; }
}
