package model;

import java.time.LocalTime;
import java.util.ArrayList;

public abstract class TipoDeMateria {
	
	public TipoDeMateria() {
		super();
	}
	
	public abstract void agregarNota(Integer nota);

	public abstract void setHorarios(Horario horarios);
	
	public abstract void setNotaFinal(int notaFinal);
	
	public abstract int getNotaFinal();
	
	public abstract ArrayList<Integer> getNotas();
	
	public abstract Horario getHorarios();
	
	public abstract LocalTime getHoraDeInicio(String dia);
	
	public abstract LocalTime getHoraDeCierre(String dia);
	
	public abstract boolean isMateriaNoCursada();
	
	public abstract boolean isMateriaCursada();
	
}
