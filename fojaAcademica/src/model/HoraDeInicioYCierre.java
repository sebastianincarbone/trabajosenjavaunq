package model;

import java.time.LocalTime;

public class HoraDeInicioYCierre {

	private LocalTime horaDeInicio;
	private LocalTime horaDeCierre;
	
	public HoraDeInicioYCierre(LocalTime horaDeInicio, LocalTime horaDeCierre) {
		this.setHoraDeInicio(horaDeInicio);
		this.setHoraDeCierre(horaDeCierre);
	}
	
	public void setHoraDeCierre(LocalTime horaDeCierre) {
		this.horaDeCierre = horaDeCierre;
	}
	
	public void setHoraDeInicio(LocalTime horaDeInicio) {
		this.horaDeInicio = horaDeInicio;
	}

	public LocalTime getHoraDeCierre() {
		return horaDeCierre;
	}
	
	public LocalTime getHoraDeInicio() {
		return horaDeInicio;
	}
	
	@Override
	public String toString() {
		return "\"Horarios\" :{ \"horaDeInicio\": " + this.horaDeInicio + ", \"horaDeCierre\": " + this.horaDeCierre + "}";
	}
}
