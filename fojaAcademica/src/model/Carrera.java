package model;

import java.util.IdentityHashMap;
import java.util.Map;

public class Carrera {

	private String nombre;
	private Map<String ,Materia> materias;
	private int cantidadDeCreditosTotales;
	
	public Carrera(String nombre) {
		this.setNombre(nombre);
		this.materias = new IdentityHashMap<>();
	}
	
	public void agregarMateria(Materia unaMateria) {
		this.materias.put(unaMateria.getNombre(), unaMateria);
	}

	private void setNombre(String nombre) {
		this.nombre = nombre;		
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getCantidadDeCreditosTotales() {
		this.cantidadDeCreditosTotales = 0;
		this.materias.values().forEach( unaMateria -> {
			 this.cantidadDeCreditosTotales += unaMateria.getCreditos();
		});
		
		return this.cantidadDeCreditosTotales;
	}

	public Materia getMateria(String nombre) {
		return this.materias.get(nombre);
	}

}