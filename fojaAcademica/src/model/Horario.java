package model;

import java.time.LocalTime;
import java.util.IdentityHashMap;
import java.util.Map;

public class Horario {
	
	private Map<String, HoraDeInicioYCierre> diaYHora;
	
	public Horario() {
		this.diaYHora = new IdentityHashMap<String, HoraDeInicioYCierre>();
	}
	
	public void setDiaYHora(String dia, LocalTime horaDeIncio, LocalTime horaDeCierre) {
		this.diaYHora.put(dia, new HoraDeInicioYCierre(horaDeIncio, horaDeCierre));
	}
	
	public HoraDeInicioYCierre getDiaYHora(String dia) {
		return this.diaYHora.get(dia);
	}
	
}
