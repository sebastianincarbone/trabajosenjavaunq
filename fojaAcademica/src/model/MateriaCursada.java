package model;

import java.time.LocalTime;
import java.util.ArrayList;

public class MateriaCursada extends TipoDeMateria{
	
	private int notaFinal;
	private ArrayList<Integer> notas;  
	private Horario horarios;
	
	public MateriaCursada(Horario horarios) {
		this.setHorarios(horarios);
	}	
	
	@Override
	public void agregarNota(Integer nota) {
		this.notas.add(nota);
	}

	@Override
	public void setHorarios(Horario horarios) {
		this.horarios = horarios;
	}

	@Override
	public void setNotaFinal(int notaFinal) {
		this.notaFinal = notaFinal;
	}

	@Override
	public int getNotaFinal() {
		return this.getNotaFinal();
	}

	@Override
	public ArrayList<Integer> getNotas() {
		return this.getNotas();
	}

	@Override
	public Horario getHorarios() {
		return this.horarios;
	}

	@Override
	public LocalTime getHoraDeInicio(String dia) {
		return this.horarios.getDiaYHora(dia).getHoraDeInicio();
	}

	@Override
	public LocalTime getHoraDeCierre(String dia) {
		return this.horarios.getDiaYHora(dia).getHoraDeCierre();
	}

	public boolean isMateriaNoCursada() { return false; }
	
	public boolean isMateriaCursada() { return true; }
}
